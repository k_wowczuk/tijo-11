# Metoda wytwórcza (wzorzec projektowy)
**Metoda wytwórcza, albo inaczej metoda fabrykująca (ang. factory method)** - jest wzorcem konstrukcyjnym. Służy do tworzenia nowych obiektów, nieokreślonych, lecz związanych z jednym, wspólnym interfejsem. Następnie dodajemy kilka klas, które będą implementować owy interfejs (to właśnie one będą tworzyć konkretne produkty). Klasy te reprezentują bardziej szczegółowe postacie produktu. Metoda fabrykująca nadaje klasom pełną odpowiedzialność, dotyczącą produkcji obiektów.

## Przykład zastosowania #1
Rozpatrzmy aplikację, której funkcjonalność może być rozszerzana za pomocą wtyczek. Może to być przeglądarka plików graficznych. Mnogość formatów graficznych sprawia, że jest prawie niemożliwym zaimplementowanie ich wszystkich naraz. Potrzebne więc jest uniwersalne rozwiązanie – takim rozwiązaniem jest system wtyczek do zapisywania i odczytywania plików graficznych. Metoda wytwórcza zwraca wskaźnik do obiektu klasy, który może manipulować obrazami danego formatu. Dzięki takiemu rozwiązaniu bezproblemowo możemy rozszerzać listę obsługiwanych formatów.

## Przykład zastosowania #2
Wyobraźmy sobie, że mamy fabrykę czekolady. W fabryce możemy produkować czekoladę gorzką, mleczną oraz czekoladę z orzechami i bakaliami. Wszystkie nasze produkty są podtypami typu czekolada. Definiujemy tutaj interfejs (czekolada) do tworzenia nowych obiektów, ale dopiero od klas, implementujących ten interfejs będzie zależeć jakiego to typu owa czekolada będzie.

## Konsekwencje stosowania
- Aplikacja wykorzystująca metody wytwórcze jest niezależna od konkretnych implementacji zasobów oraz procesu ich tworzenia. Mogą być one ustalane dynamicznie w trakcie uruchomienia lub zmieniane podczas działania aplikacji.
- Wzorzec hermetyzuje proces tworzenia obiektów, zamykając go za ściśle zdefiniowanym interfejsem. Właściwość ta jest wykorzystywana, gdy tworzenie nowego obiektu jest złożoną operacją (np. wymaga wstrzyknięcia dodatkowych zależności).
- W wielu obiektowych językach programowania konstruktory klas muszą posiadać ściśle określone nazwy, co może być źródłem niejednoznaczności podczas korzystania z nich. Wzorzec umożliwia zastosowanie nazwy opisowej oraz wprowadzenie kilku metod fabryki tworzących obiekt na różne sposoby.

## Struktura wzorca
We wzorcu występują dwie ogólne klasy bądź interfejsy definiujące pewien typ zasobów (Product) oraz sposób ich tworzenia (Creator, metoda factoryMethod()). Od nich wyprowadza się konkretne klasy zasobów (ConcreteProduct) wraz z tworzącymi je klasami wytwórczymi (ConcreteCreator), które dostarczają odpowiednią implementację metody factoryMethod(). Komponent pragnący tworzyć zasoby i operować na nich, korzysta z ogólnych interfejsów Product oraz Creator, umożliwiając wybór konkretnej implementacji w sposób dynamiczny

![alt text](http://zasoby.open.agh.edu.pl/~09sbfraczek/images/wzorce/12.png)
![alt text](http://zasoby.open.agh.edu.pl/~09sbfraczek/images/wzorce/13.png)

## Stosowalność metod wytwórczych
- klasa nie może przewidzieć, jakich klas obiekty musi tworzyć
- klasa chce, by to jej podklasy specyfikowały tworzone przez nią obiekty
- klasy przekazuj? odpowiedzialność jednej z kilku podklas pomocniczych, a Ty chcesz tylko w jednym miejscu ustalić, która z podklas pomocniczych jest ich delegatem

### Linki:
 - https://pl.wikipedia.org/wiki/Metoda_wytw%C3%B3rcza_(wzorzec_projektowy)
 - http://zasoby.open.agh.edu.pl/~09sbfraczek/metoda-wytworcza%2C1%2C29.html
 - http://www.algorytm.org/wzorce-projektowe/metoda-wytworcza-factory-method.html#impl630